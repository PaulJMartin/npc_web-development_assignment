# NPC_Web-Development_Assignment

Project based around developing a fully functional website for a fiction pet clinic, which enabled users to learn about the clinic and its services, as well as register their details. 

I used HTML 5, CSS 3 and JavaScript for the front-end and for the back-end I used PHP to connect my HTML forms to the customer registration and treatments tables, found within the clinics database on my XAMPP server.