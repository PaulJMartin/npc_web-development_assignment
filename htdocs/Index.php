<DOCTYPE! html>
       <head>
	        <title>   NPC | Welcome! </title>
			<link type="text/css" rel="stylesheet" href="style.css">
			<meta charset="UTF-8">
			 <meta name="viewport" content="width=device-width, initial-scale=1.0">  
       </head>
       <body>
	         	
	       
			<div id="container">
			 
			<div id = "Welcome">
			<a id = "Section1"> </a>
			        <div id = "WelcomeLogo">
                           <img src="Images\NPC_Final_Logo.png" alt="Logo">
                  </div>
			  <a id = "Section2"></a>
			</div>
			
			<div id="AnimatedBanner">
			  <div id="AnimatedContent">
			  <h3> Contact Details: &nbsp  Email: Contact@NPC.com  &nbsp&nbsp&nbsp  Phone: 01352 786087  &nbsp&nbsp&nbsp&nbsp|&nbsp&nbsp&nbsp&nbsp  Services:&nbsp  ⚪ General Health Check   ⚪ Vaccination   ⚪ Surgery &nbsp &nbsp|&nbsp &nbsp Products: &nbsp ⚪  Pet Medication</h3>
			</div>
			</div>
			
	 <div id = "Banner">
			    		   
			      
				 
				       <ul class = "topnav">
					      
				          
					
						   <li class = "TextNav"><a href ="#Section1"><img id = "HomeButton" src ="Images\Home-Icon.png" alt="HomeButton"></a><li>
						    <li class = "TextNav"><a href ="#Section2">SlideShow</a></li>
					       <li class = "TextNav"><a href ="#Section4">About</a></li>
					       <li class = "TextNav"><a href ="#Section5">Services</a></li>
					       <li class = "TextNav"><a href ="#Section6">Registration</a></li>				        
						   <li class = "TextNav"><a href ="#Section7">Map</a></li>
			
				      </ul> 
					  
           	
				
			</div>
			
	
			<div id = "Slideshow">
			
			
			<div id="SlideshowContent">
              <img class="mySlides" src="Images\banner01.jpg" alt = "Appointment Banner"/>
			  <img class="mySlides" src="Images\S15f-Vet-Banner-Master01b.jpg" alt = "Contact Banner"/>
			  <img class="mySlides" src="Images\Contact.png" alt ="ContactSheet, please see contact section for more details"/>
			  <img class="mySlides" src="Images\Services.png" alt = "Services, please see services section for more details"/>
			  <img class="mySlides" src="Images\PetMeds.png" alt = "Pet Medication Available"/>
			  
              <img class="mySlides" src="Images\20130711160422786_website_services_122043798-1500x1000.jpg" alt = "Dog Image"/>
              <img class="mySlides" src="Images\Guinea_Pig.png" alt = "Guniea_Pig Image"/>
              <img class="mySlides" src="Images\Martha-Lake-Veterinary-Lynnwood-Cat.jpg" alt = "Cat Image"/>
			</div>
			

			<a id = "Section4"> </a>
				  
			      
			</div>
			
			
			
			
			<div id = "About">
			    <div id = "AboutText">
			        <h1>About Us</h1>
					<p> ⚪ Noah's Pet Clinic, is a Manchester based vets clinic established since 1978. <br><br><br>

⚪ Since 2014 we have been taken over by Independent Vet Care, enabling us to take advantage of the positives of the bigger companies but still run as an independent practice.<br><br><br>

⚪ We have high standard pet grooming facilities on site.<br><br><br>

⚪ We provide veterinary services for companion animals (pets); dogs, cats, rabbits and other small animals, as well as the more exotic pets routinely presented to us.<br><br><br>

⚪ We have ten highly qualified vets and 8 qualified nurses who uphold high nursing standards for our hospitalized cases. We take an on-going approach to professional development and all members of staff regularly attend training courses.<br><br><br>

⚪ Our aim is to work with our clients to provide our patients with as long, healthy and active a life as possible.<br><br><br></p>
				</div>
                				
			        <a id = "Section5"> </a>
			</div>
			
			<div id = "Services">
			        <h1>Our Services</h1>
					
					<div id = "TheServicesContents">
					  
					  <div class = "ServicesSections">
					  <img id = "HealthCheck" src = "Images\HCheck.png" alt = "Health Check Image"/>
					  <h3>Health Checks</h3>
					  <p>Welcoming a new pet into your life and home is a special occasion and should be cherished for years to come. It’s also incredibly important to ensure your young pet gets the best start in life. One of the best ways to do so is by visiting our team regularly for the first six months of their life.
                      <br><br>
                     
                      </p>
					  </div>
					
					  <div class = "ServicesSections">
					  <img id = "Vaccination" src = "Images\Vacc.png" atl = "Vaccination Image"/>
					  <h3>Vaccinations</h3>
					  <p>Vaccinations are important injections that are given to puppies, kittens and baby rabbits to prevent them from getting nasty diseases and infections. Before your pet is allowed to interact with other animals or go outside, they should have completed a course of vaccinations.
                      <br><br>
                      </p>
					  </div>
					
					  <div class = "ServicesSections">
					  <img id = "Surgery" src = "Images\Surgery.png" alt = "Surgery Image"/>
					  <h3>Soft tissue surgery</h3>
					  <p>At Noah's Pet Clinic we conduct a wide range of soft tissue surgery in house by our own highly trained an experienced veterinary surgeons. These procedures include lump removal, abdominal surgery, thoracic surgery as well as minor eye surgery. We may be able to offer other services.
                      <br><br>
                       </p>
					  </div>
					<a id="formSubmition"></a>
					</div> 
                  <?php
$conn = mysqli_connect("localhost","root","", "noah");
	if (!$conn) {
		die ("");
	}
	echo "";
?> 
				  
				  <div id = "ServiceEnquiriesContainer">
				  
                      <div id = "ServiceEnquiries">
					    <h2>Treatment Enquiries</h2>
						  <p>Please Enter the details below to gain treatment pricing: </p> <br>
                          <p>⚪ General Health Check   ⚪ Vaccination   ⚪ Surgery<p> <br>
			
			
		                    <form action = "Index.php#formSubmition" method = "POST" autocomplete = "on">                 				      						  
						   Treatment type: 
						   <select name="Treatment">
  <option value="General Health Check">General Health Check</option>
  <option value="Vaccination">Vaccination</option>
  <option value="Surgery">Surgery</option>
  
</select> 
							 <br> <br> <br>
                           <input type = "Submit" Value = "Submit"/> <br><br>
                   
                     </form>	
												
			
			<?php 
		error_reporting(0);
			$sql = "SELECT * FROM treatments WHERE (Treatment ='".$_POST["Treatment"]."')";
		$result = $conn->query($sql);
		if ($result->num_rows > 0){
		while ($row = $result->fetch_assoc()){

			
		echo($_POST["Treatment"]."<h3> Cost:   &pound".$row["Cost"]."</h3>");}}
        else {echo("<h3> </h3>");}
		?>
					   
			          </div>
				</div>
                           					
					    <a id = "Section6"> </a>
				</div>	
			
			
			<div id = "Registration">
			
			
			<div id = "RegContainerParent">
			
			<div id = "contactDetails">
			   
			      <h1>Contact Details</h1>
				  
				  <div id = "ContactDetailsContainer">
				  
				  
				  <h3>Phone</h3>  
				  <p>01352 786087</p>
				  
				  <h3>Email</h3>
				  <p>Contact@NPC.com</p>
                  

                  </div>				  
			
			</div>
			
			    <div id ="RegContainer">
				
				<h2>Registration</h2>
						  <p>Please Enter your details below: </p>
                     
                     
					      
					
                           <form action = "Index.php#Section6" method = "POST"> 
Name:<br><input type="text" name ="Name"><br>
Address:<br><input type="text" name ="Address"><br>
PostCode:<br><input type="text" name ="PostCode"><br>
Email:<br><input type="text" name ="Email"><br><br>
<input name = "SubmitFeedback" type = "submit" value = "Register"/></form><br>

						   
                   
					 
          
					 <?php 
if (isset($_POST['SubmitFeedback'] )) {
		   $sql= "INSERT INTO customer (Name, Address, PostCode, Email)
		        VALUES ('".$_POST["Name"]."', '".$_POST["Address"]."', '".$_POST["PostCode"]."', '".$_POST["Email"]."')";

if ($conn->query($sql)){
echo ("<h3>Welcome ".$_POST["Name"]."!</h3>");}}
else {echo("");}?>

                     
			
                </div>
				
				
				
				</div>
			        <a id = "Section7"> </a>
			</div>
			
			<div id = "MapSection">
			
			    <div id = "MapContainer">
				     <div id = "map">
					 </div>
				</div>
			        
			</div>
			
			<div id = "Footer">
			<div id = "Copyright">
			      <p> © 2017 | Paul Martin - Web Services </p>
			</div>
			
			</div>
			
			
			</div>
			
<script>
var myIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    myIndex++;
    if (myIndex > x.length) {myIndex = 1}    
    x[myIndex-1].style.display = "block";  
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
</script>

<script>
function myMap() {
  var myCenter = new google.maps.LatLng(53.406913, -2.160348);
  var mapCanvas = document.getElementById("map");
  var mapOptions = {center: myCenter, zoom: 7};
  var map = new google.maps.Map(mapCanvas, mapOptions);
  var marker = new google.maps.Marker({position:myCenter});
  marker.setMap(map);

  // Zoom to 9 when clicking on marker
  google.maps.event.addListener(marker,'click',function() {
    map.setZoom(15);
    map.setCenter(marker.getPosition());
  });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAzvJULIf_aKTcTuzAOo6KidqJOoqhqGeI&callback=myMap"></script>
			
		
			
       </body>
  </html>
