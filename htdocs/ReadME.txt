This is the instructions for my website
---------------------------------------

1) When browsing for the site, I have had to enter the port number of my Apache server.

For example on my server I used the following URL: http://localhost:8090/

This url has enabled me to load the site on my server.


2) All my HTML/JavaScript/PHP code can be found within (Index.PHP)
   All my CSS code can be found within (Style.CSS) 

3) When creating the database within PHPMyAdmin, please make sure the name of the database is:   noah

4) The website functionality works on the following browsers:

* Firefox
* Chrome

